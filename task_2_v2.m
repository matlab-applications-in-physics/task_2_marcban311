%i save data as txt and i change comma into point
Data = fileread("dane-pomiarowe_2020-11-02.csv");
Data = strrep(Data, ',', '.');
FID = fopen('2020-11-02.txt', 'w');
fwrite(FID, Data, 'char');
fclose(FID);

Data = fileread("dane-pomiarowe_2020-11-03.csv");
Data = strrep(Data, ',', '.');
FID = fopen('2020-11-03.txt', 'w');
fwrite(FID, Data, 'char');
fclose(FID);

Data = fileread("dane-pomiarowe_2020-11-04.csv");
Data = strrep(Data, ',', '.');
FID = fopen('2020-11-04.txt', 'w');
fwrite(FID, Data, 'char');
fclose(FID);

Data = fileread("dane-pomiarowe_2020-11-05.csv");
Data = strrep(Data, ',', '.');
FID = fopen('2020-11-05.txt', 'w');
fwrite(FID, Data, 'char');
fclose(FID);

Data = fileread("dane-pomiarowe_2020-11-06.csv");
Data = strrep(Data, ',', '.');
FID = fopen('2020-11-06.txt', 'w');
fwrite(FID, Data, 'char');
fclose(FID);

Data = fileread("dane-pomiarowe_2020-11-07.csv");
Data = strrep(Data, ',', '.');
FID = fopen('2020-11-07.txt', 'w');
fwrite(FID, Data, 'char');
fclose(FID);

Data = fileread("dane-pomiarowe_2020-11-08.csv");
Data = strrep(Data, ',', '.');
FID = fopen('2020-11-08.txt', 'w');
fwrite(FID, Data, 'char');
fclose(FID);

Data = fileread("dane-pomiarowe_2020-11-09.csv");
Data = strrep(Data, ',', '.');
FID = fopen('2020-11-09.txt', 'w');
fwrite(FID, Data, 'char');
fclose(FID);

Data = fileread("dane-pomiarowe_2020-11-10.csv");
Data = strrep(Data, ',', '.');
FID = fopen('2020-11-10.txt', 'w');
fwrite(FID, Data, 'char');
fclose(FID);

Data = fileread("dane-pomiarowe_2020-11-11.csv");
Data = strrep(Data, ',', '.');
FID = fopen('2020-11-11.txt', 'w');
fwrite(FID, Data, 'char');
fclose(FID);

Data = fileread("dane-pomiarowe_2020-11-12.csv");
Data = strrep(Data, ',', '.');
FID = fopen('2020-11-12.txt', 'w');
fwrite(FID, Data, 'char');
fclose(FID);

Data = fileread("dane-pomiarowe_2020-11-13.csv");
Data = strrep(Data, ',', '.');
FID = fopen('2020-11-13.txt', 'w');
fwrite(FID, Data, 'char');
fclose(FID);

Data = fileread("dane-pomiarowe_2020-11-14.csv");
Data = strrep(Data, ',', '.');
FID = fopen('2020-11-14.txt', 'w');
fwrite(FID, Data, 'char');
fclose(FID);

Data = fileread("dane-pomiarowe_2020-11-15.csv");
Data = strrep(Data, ',', '.');
FID = fopen('2020-11-15.txt', 'w');
fwrite(FID, Data, 'char');
fclose(FID);

%i import the data from excel to concert int to matrix (only for the values with hour)
A_1=readmatrix("Matlab\air_quality_data_Zabrze\2020-11-02.txt",'Range','B2:J25');
A_2=readmatrix("Matlab\air_quality_data_Zabrze\2020-11-03.txt",'Range','B2:J25');
A_3=readmatrix("Matlab\air_quality_data_Zabrze\2020-11-04.txt",'Range','B2:J25');
A_4=readmatrix("Matlab\air_quality_data_Zabrze\2020-11-05.txt",'Range','B2:J25');
A_5=readmatrix("Matlab\air_quality_data_Zabrze\2020-11-06.txt",'Range','B2:J25');
A_6=readmatrix("Matlab\air_quality_data_Zabrze\2020-11-07.txt",'Range','B2:J25');
A_7=readmatrix("Matlab\air_quality_data_Zabrze\2020-11-08.txt",'Range','B2:J25');
A_8=readmatrix("Matlab\air_quality_data_Zabrze\2020-11-09.txt",'Range','B2:J25');
A_9=readmatrix("Matlab\air_quality_data_Zabrze\2020-11-10.txt",'Range','B2:J25');
A_10=readmatrix("Matlab\air_quality_data_Zabrze\2020-11-11.txt",'Range','B2:J25');
A_11=readmatrix("Matlab\air_quality_data_Zabrze\2020-11-12.txt",'Range','B2:J25');
A_12=readmatrix("Matlab\air_quality_data_Zabrze\2020-11-13.txt",'Range','B2:J25');
A_13=readmatrix("Matlab\air_quality_data_Zabrze\2020-11-14.txt",'Range','B2:J25');
A_14=readmatrix("Matlab\air_quality_data_Zabrze\2020-11-15.txt",'Range','B2:J22');

%one from all 
eThing=[A_1; A_2; A_3; A_4; A_5; A_6; A_7; A_8; A_9; A_10; A_11; A_12; 
    A_13; A_14]

dlmwrite('GetToTheChopa.txt',eThing);

X= readmatrix("GetToTheChopa.txt")

%defining time and date
t0 = datenum('2020-11-02 00:00');
dt = 1/24; % 1 hour
t = datestr(t0 + (1:(13.90*24))*dt)



